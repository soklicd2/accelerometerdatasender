package com.example.domen.accelerometerdatasender;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.os.AsyncTask;
import android.os.Build;

public class UdpSender
{
    private AsyncTask<Void, Void, Void> async_cient;

    public void SendMessage(final String address, final int port, final String message)
    {
        async_cient = new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... params)
            {
                DatagramSocket ds = null;

                try
                {
                    ds = new DatagramSocket();
                    DatagramPacket dp;
                    dp = new DatagramPacket(message.getBytes(), message.length(), InetAddress.getByName(address), port);
                    ds.setBroadcast(true);
                    ds.send(dp);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    if (ds != null)
                    {
                        ds.close();
                    }
                }
                return null;
            }

            protected void onPostExecute(Void result)
            {
                super.onPostExecute(result);
            }
        };

        if (Build.VERSION.SDK_INT >= 11){
            async_cient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            async_cient.execute();
        }
    }
}