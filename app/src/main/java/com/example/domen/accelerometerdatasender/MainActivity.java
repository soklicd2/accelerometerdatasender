package com.example.domen.accelerometerdatasender;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity implements SensorEventListener {
    boolean running = false;
    final int sendingInterval = 1000;
    EditText xAccelEditText;
    EditText yAccelEditText;
    EditText zAccelEditText;

    EditText addressEditText;
    EditText portEditText;

    UdpSender dataSender;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addressEditText = (EditText) findViewById(R.id.editTextUrl);
        portEditText = (EditText) findViewById(R.id.editTextPort);

        yAccelEditText = (EditText) findViewById(R.id.editTextY);
        xAccelEditText = (EditText) findViewById(R.id.editTextX);
        zAccelEditText = (EditText) findViewById(R.id.editTextZ);

        final Button startStop = (Button) findViewById(R.id.button);

        dataSender = new UdpSender();

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);

        startStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(running){
                    // stop
                    running = false;
                    startStop.setText("Start");

                    addressEditText.setEnabled(true);
                    portEditText.setEnabled(true);
                } else {
                    // start
                    running = true;
                    startStop.setText("Stop");

                    addressEditText.setEnabled(false);
                    portEditText.setEnabled(false);
                }

            }
        });
    }

    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void send(){
        String targetURL = addressEditText.getText().toString().trim();
        int targetPort = Integer.parseInt(portEditText.getText().toString().trim());

        String timestamp = String.valueOf(System.currentTimeMillis()/1000.0);

        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("ACC: FFFFFFFFB6AAFCF2CB814C19B0B6B4A30A680CD8,")
                .append(timestamp)
                .append(",")
                .append(xAccelEditText.getText().toString().trim())
                .append(",")
                .append(yAccelEditText.getText().toString().trim())
                .append(",")
                .append(zAccelEditText.getText().toString().trim());

        dataSender.SendMessage(targetURL, targetPort, messageBuilder.toString());
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            xAccelEditText.setText(String.valueOf(x));
            yAccelEditText.setText(String.valueOf(y));
            zAccelEditText.setText(String.valueOf(z));

            if(running){
                send();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
