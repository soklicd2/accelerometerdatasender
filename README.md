# Overview #

This Android application sends accelerometer data to an iPhone emulator.
This code is to be used with [this application](https://code.google.com/p/accelerometer-simulator/).

# Using the application #

First add **AccelerometerSimulation.m** and **AccelerometerSimulation.h** to your iOS project. Install this app and run it.

Insert the IP of your Mac/Simulator and the port on which to send the data.

Press Start.